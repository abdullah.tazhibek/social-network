import './App.css';
import {Header} from "./components/Header/Header";
import {Profile} from "./components/Profile/Profile";
import {Route, Routes} from "react-router-dom";
import {Navbar} from "./components/Navbar/Navbar";
import {Home} from "./components/Home/Home";
import {Music} from "./components/Music/Music";
import {Settings} from "./components/Settings/Settings";
import {DialogsContainer} from "./components/Dialogs/DialogsContainer";

export const App = ({state}) => {
    return (
        <div className='app-wrapper'>
            <Header/>
            <Navbar/>
            <div className={"app-wrapper-content"}>
                <Routes>
                    <Route path='/' element={<Home />}/>
                    <Route path={'/profile'} element={<Profile />}/>
                    <Route path={'/dialogs'} element={<DialogsContainer />}/>
                    <Route path={'/music'} element={<Music />}/>
                    <Route path={'/settings'} element={<Settings />}/>
                </Routes>
            </div>
        </div>
    );
}
