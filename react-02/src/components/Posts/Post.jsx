import vis from './Post.module.css';
import icon from '../../logo.svg'

export const Post = ({text, comCount, ...props}) => {
    return (
        <div className={vis.item} key={props.key}>
            <img src={icon} width={"20px"} height={"20px"} alt={"logo"}/>
            {' '+text+' '}
            {props.likeCount}
            <button>Like</button>
            {comCount}
            <button>Comment</button>
            {props.shareCount}
            <button>Share</button>
            <button>Save</button>
        </div>
    );
}