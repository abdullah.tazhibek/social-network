import {Post} from "../../Posts/Post";
import React, {useState} from "react";
import {addPostActionCreator, updateNewPostTextActionCreator} from "../../../redux/profile-reducer";
import {store} from "../../../redux/redux-store";

export const MyPosts = () => {
    const myPosts = store.getState().profilePage.myPosts
    const [text, setText] = useState('')

    let addPost = () => {
        let action = addPostActionCreator()
        store.dispatch(action)
        setText('')
    }

    let onPostChange = (value) => {
        setText(value)
        let action = updateNewPostTextActionCreator(value)
        store.dispatch(action)
    }

    return (
        <div>
            <textarea
                onChange={(event)=>onPostChange(event.target.value)}
                value={text}
            />
            <button onClick={()=>addPost()}>Add post</button>
            { myPosts.map( (p,index) =>
            <Post key={index} text={p.text} likeCount={p.likeCount} comCount={p.comCount} shareCount={p.shareCount}/>) }
        </div>
    );
}