import {store} from "../../../redux/redux-store";
import React from "react";
export const Message = () => {
    const messages = store.getState().dialogsPage.messages

    return (
        <div>
            { messages?.map ((m,index) => <div key={index}>{m.message}</div>) }
        </div>
    )
}