import s from './Dialogs.module.css';
import React from "react";
import {UserDialog} from "./UserDialog/UserDialog";
import {Message} from "./Message/Message";

export const Dialogs = (props) => {
    let onMessageSendClick = () => {
        props.addMessage()
    }

    let onNewMessageChange = (event) => {
        let newMessage = event.target.value
        props.updateNewMessageText(newMessage)
    }

    return (
        <div className={s.dialogs}>
            <UserDialog />
            <div className={s.messages}> <Message />
                <div>
                    <textarea
                        value={props.newMessage}
                        onChange={onNewMessageChange}
                        placeholder={"text"}/>
                    <button onClick={onMessageSendClick}>Send message</button>
                </div>
            </div>
        </div>
    );
}