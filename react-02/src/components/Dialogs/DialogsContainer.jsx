import React from "react";
import {addMessageCreator, updateNewMessageTextCreator} from "../../redux/dialogs-reducer";
import {store} from "../../redux/redux-store";
import {Dialogs} from "./Dialogs";

export const DialogsContainer = () => {
    let state = store.getState().dialogsPage

    let onMessageSendClick = () => {
        console.log("text is here")
        store.dispatch(addMessageCreator())
    }

    let onNewMessageChange = (newMessage) => {
        let action = updateNewMessageTextCreator(newMessage)
        store.dispatch(action)
    }

    return <Dialogs updateNewMessageText={onNewMessageChange} addMessage={onMessageSendClick} dialogsPage={state}/>
}