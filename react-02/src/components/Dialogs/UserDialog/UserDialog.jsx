import {NavLink} from "react-router-dom";
import s from "../Dialogs.module.css";
import {store} from "../../../redux/redux-store";
import React from "react";

export const UserDialog = () => {
    const dialogs = store.getState().dialogsPage.dialogs
    let path = "/dialogs/" + dialogs.id

    return (
        <div>
            <div className={s.dialogItems}>
                { dialogs?.map ((d,index) =>
                    <a key={index} href={''}>{d.name}</a>) }
            </div>
            <div className={s.dialogItems + ' ' + s.active}>
                <NavLink to={path}>{dialogs.id}</NavLink>
            </div>
        </div>
    )
}