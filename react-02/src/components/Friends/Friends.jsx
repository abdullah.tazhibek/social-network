import vis from './Friends.module.css'
import {store} from "../../redux/redux-store";

export const Friends = () => {
    const sidebar = store.getState().friendsBlock.sidebar

    return (
        <div className={vis.friends_block}>
            Friends
            { sidebar && sidebar.map ((p,index) => <p key={index}>{p.img} {p.name}</p>) }
        </div>
    );
}