import {Post} from "../Posts/Post";
import {store} from "../../redux/store";

export const Home = () => {
    const mainPosts = store.getState().homePage.mainPosts

    return (
        <div>
            { mainPosts.map ((p,index) =>
                <Post key={index} text={p.text} likeCount={p.likeCount}
                      comCount={p.comCount} shareCount={p.shareCount}/>)
            }
        </div>
    );
}