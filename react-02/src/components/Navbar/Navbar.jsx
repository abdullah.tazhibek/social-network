import vis from './Navbar.module.css';
import {NavLink} from "react-router-dom";
import {Friends} from "../Friends/Friends";

export const Navbar = () => {
    return (
        <nav className={vis.nav}>
            <NavLink className={vis.item} to={'/'}>Home</NavLink>
            <NavLink className={vis.item} to={'/profile'}>Profile</NavLink>
            <NavLink className={vis.item} to={'/dialogs'}>Dialogs</NavLink>
            <NavLink className={vis.item} to={'/music'}>Music</NavLink>
            <NavLink className={vis.item} to={'settings'}>Settings</NavLink>
            <Friends className={vis.sidebar}/>
        </nav>
    );
}