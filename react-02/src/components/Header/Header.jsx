import vis from './Header.module.css';

export const Header = () => {
    return (
        <header className={vis.header}>
            <img src="" alt="logo"/>
        </header>
    );
}
