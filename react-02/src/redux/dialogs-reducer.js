const ADD_MESSAGE = 'ADD-MESSAGE';
const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE-NEW-MESSAGE-TEXT';

let initialState = ({
    dialogs : [
        {id: 1, name: "Pete"},
        {id: 2, name: "Andrew"},
        {id: 3, name: "Colin"},
        {id: 4, name: "John"},
    ],
    messages : [
        {id: 1, message: "Hello buddy"},
        {id: 2, message: "I have seen you near Luka Mall"},
        {id: 3, message: "Yo bro"},
    ],
    newMessage : '',
})

export const dialogsReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_MESSAGE:
            state.messages.push({id: 5, message: state.newMessage})
            state.newMessage = ''
            return state
        case UPDATE_NEW_MESSAGE_TEXT:
            state.newMessage = action.value
            return state
        default:
            return state
    }
}

export const addMessageCreator = () => ({type: ADD_MESSAGE})
export const updateNewMessageTextCreator = (newMessage) =>
    ({type: UPDATE_NEW_MESSAGE_TEXT, value: newMessage})