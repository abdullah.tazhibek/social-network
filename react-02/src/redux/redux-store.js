import {combineReducers, createStore} from "redux";
import {profileReducer} from "./profile-reducer";
import {dialogsReducer} from "./dialogs-reducer";
import {homeReducer} from "./home-reducer";
import {friendsReducer} from "./friends-reducer";

const reducers = combineReducers({
    profilePage: profileReducer,
    dialogsPage: dialogsReducer,
    homePage: homeReducer,
    friendsBlock: friendsReducer
})

export const store = createStore(reducers)