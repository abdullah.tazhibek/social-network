import {profileReducer} from "./profile-reducer";
import {dialogsReducer} from "./dialogs-reducer";
import {homeReducer} from "./home-reducer";
import {friendsReducer} from "./friends-reducer";

export let store = {
    _callSubscriber() {
        console.log("State changed")
    },
    _state: {
        homePage : {
            mainPosts : [
                {text: "Long time no see, friends", likeCount: 15, comCount: 20, shareCount: 5},
                {text: "How many times you use gadget?", likeCount: 7, comCount: 31, shareCount: 1},
                {text: "Gadget technology", likeCount: 7, comCount: 31, shareCount: 1},
            ],
        },
        dialogsPage : {
            dialogs : [
                {id: 1, name: "Pete"},
                {id: 2, name: "Andrew"},
                {id: 3, name: "Colin"},
                {id: 4, name: "John"},
            ],
            messages : [
                {id: 1, message: "Hello buddy"},
                {id: 2, message: "I have seen you near Luka Mall"},
                {id: 3, message: "Yo bro"},
            ],
            newMessage : "",
        },
        profilePage : {
            myPosts : [
                {id: 1, text: "Long time no see, friends", likeCount: 15, comCount: 20, shareCount: 5},
                {id: 2, text: "How many times you use gadget?", likeCount: 7, comCount: 31, shareCount: 1},
                {id: 3, text: "Honey", likeCount: 7, comCount: 31, shareCount: 1},
                {id: 4, text: "How?", likeCount: 7, comCount: 31, shareCount: 1},
            ],
            newPostText : []
        },
        friendsBlock : {
            sidebar : [
            {id: 1, img: "", name: 'Andrew'},
            {id: 2, img: "", name: 'Katya'},
            {id: 3, img: "", name: 'Wilson'},
            ]
        }
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer
    },

    dispatch(action) {
        this._state.profilePage = profileReducer(this._state.profilePage, action)
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action)
        this._state.homePage = homeReducer(this._state.homePage, action)
        this._state.friendsBlock = friendsReducer(this._state.friendsBlock, action)

        this._callSubscriber(this._state)
    }
}