const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';

let initialState = ({
    myPosts : [
        {id: 1, text: "Long time no see, friends", likeCount: 15, comCount: 20, shareCount: 5},
        {id: 2, text: "How many times you use gadget?", likeCount: 7, comCount: 31, shareCount: 1},
        {id: 3, text: "Honey", likeCount: 7, comCount: 31, shareCount: 1},
        {id: 4, text: "How?", likeCount: 7, comCount: 31, shareCount: 1},
    ],
    newPostText : []
})

export const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            let newPostMessage = {
                id: 5,
                text: state.newPostText,
                likeCount: 0,
                comCount: 0,
                shareCount: 0
            }
            state.myPosts.push(newPostMessage)
            state.newPostText = ''
            return state
        case UPDATE_NEW_POST_TEXT:
            state.newPostText = action.value
            return state
        default:
            return state
    }
}

export const addPostActionCreator = () => ({type: ADD_POST})
export const updateNewPostTextActionCreator = (value) =>
    ({type: UPDATE_NEW_POST_TEXT, value: value})